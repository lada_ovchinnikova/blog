import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];
  displayedColumns: string[] = ['position','title', 'author', 'actions']

  constructor(
    private readonly postsService: PostsService
  ) { }

  ngOnInit(): void {
    this.postsService.find().subscribe(
      (posts: Post[]) => this.posts = posts
    );
  }
  remove(id: string): void {
    this.postsService.delete(id).subscribe(
      () => this.posts = this.posts.filter(
        (post: Post) => post.id !==id
      )
    );
  }

}
