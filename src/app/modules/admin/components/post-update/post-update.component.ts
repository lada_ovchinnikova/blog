import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-update',
  templateUrl: './post-update.component.html',
  styleUrls: ['./post-update.component.scss']
})
export class PostUpdateComponent implements OnInit {
  id: string = "";
  post: Post = null;

  form: FormGroup = new FormGroup({
    author: new FormControl(null),
    title: new FormControl(null),
    content: new FormControl(null),
    date: new FormControl(null)
  })
  
  constructor (
    private route: ActivatedRoute,
    private readonly postsService: PostsService
  ) { 
  }

  ngOnInit(): void {
    this.load()
    this.route.params.subscribe((params: Params)=> this.id = params.id)
  }

  private load(): void {
    this.route.params.pipe(
      switchMap((params: Params) => this.postsService.findOne(params.id).pipe(
        tap((post: Post) => this.post = post)
      ))
    ).subscribe();
    }
    addData(): void {
      this.form.setValue({
        title: this.post.title,
        author: this.post.author,
        content: this.post.content,
        date: this.post.date
      })
    }
  submit(): void {
    const post: Post = this.form.value;
    this.postsService.create(post).subscribe();
  }
  upload(id: string): void {
    const post: Post = this.form.value;
    this.postsService.update(id, post).subscribe();
  }

}
