import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-creator',
  templateUrl: './post-creator.component.html',
  styleUrls: ['./post-creator.component.scss']
})
export class PostCreatorComponent implements OnInit {
  form: FormGroup = new FormGroup({
    author: new FormControl(null),
    title: new FormControl(null),
    content: new FormControl(null),
    date: new FormControl(null)
  })
  
  constructor(
    private readonly postsService: PostsService
  ) { }

  ngOnInit(): void {
  }

  submit(): void {
    const post: Post = this.form.value;
    this.postsService.create(post).subscribe();
  }
}
