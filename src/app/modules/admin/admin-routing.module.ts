import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { PostCreatorComponent } from './components/post-creator/post-creator.component';
import { PostUpdateComponent } from './components/post-update/post-update.component';
import { PostComponent } from './components/post/post.component';
import { PostsComponent } from './components/posts/posts.component';


const routes: Routes = [
  {  path: 'posts', component: PostsComponent, canActivate: [AuthGuard] },
  {  path: 'posts/create', component: PostCreatorComponent, canActivate: [AuthGuard] },
  {  path: 'posts/:id', component: PostComponent, canActivate: [AuthGuard] },
  {  path: 'posts/:id/update', component: PostUpdateComponent, canActivate: [AuthGuard] },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
