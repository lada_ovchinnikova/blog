import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FBAuthResponce } from '../interfa/auth-res';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  get token(): string {
    const expDate: string = localStorage.getItem('DATE_EXP');
    if(!expDate) {
      return null;
    }

    if(new Date() > new Date(expDate)) {
      this.logout();
      return null;
    }
    return localStorage.getItem('ACESS_TOKEN');
  }
  constructor(
    private readonly router: Router,
    private readonly httpClient: HttpClient
  ) { }

  getEmail(): string {
    return localStorage.getItem('EMAIL');
  }
  
  isAuthenticated(): boolean {
    return !!this.token;
  }

  login(email: string, password: string) {
    return this.httpClient.post<FBAuthResponce>(
      `${environment.authUrl}/accounts:signInWithPassword?key=${environment.apiKey}`,
      {email, password, returnSecureToken: true },
      {
        headers: {
          'Content-type': 'application/json'
        }
      }
    ).pipe(
      tap((res: FBAuthResponce) => {
        const {email, expiresIn, idToken} = res;
        if(!idToken || !expiresIn) {
          return;
        }

        const expDate: Date = new Date(new Date().getTime() + +expiresIn * 1000)

        localStorage.setItem('ACESS_TOKEN',idToken);
        localStorage.setItem('DATE_EXP',expDate.toString());
        localStorage.setItem('EMAIL',email);
      })
    )
  }

  logout(): void {
    localStorage.clear();
    this.router.navigateByUrl('/auth/sigh-in');
  }
}
