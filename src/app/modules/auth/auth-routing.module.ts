import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SighInComponent } from './components/sigh-in/sigh-in.component';

const routes: Routes = [
  {  path: 'sigh-in', component: SighInComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
