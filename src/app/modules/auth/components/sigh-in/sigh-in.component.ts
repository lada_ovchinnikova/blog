import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sigh-in',
  templateUrl: './sigh-in.component.html',
  styleUrls: ['./sigh-in.component.scss']
})
export class SighInComponent implements OnInit {
  form: FormGroup = new FormGroup({
      email: new FormControl('example@mail.ru', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('123Malta', [
        Validators.required,
        Validators.minLength(6)
      ]) 
  })
  error: string;


  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) { }

  ngOnInit(): void {
  }
  submit(): void {
    const { email, password } = this.form.value;
    this.authService.login(email, password ).pipe(
      tap(() => this.router.navigateByUrl('/admin/posts'))
    ).subscribe();
  }

}
