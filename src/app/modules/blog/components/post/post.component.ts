import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  id: string = "";
  post: Post = null;

  constructor(
    // private router: Router,
    private route: ActivatedRoute,
    private postsService: PostsService
  ) { }

  ngOnInit(): void {
   this.load()
  }
  private load(): void {
    this.route.params.pipe(
      switchMap((params: Params) => this.postsService.findOne(params.id).pipe(
        tap((post: Post) => this.post = post)
      ))
    ).subscribe();
    }
}
