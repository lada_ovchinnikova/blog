export interface Post {
  author: string;
  title: string;
  content: string;
  date: string;
  id: string;
}
