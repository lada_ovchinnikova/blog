import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  {  path: '', redirectTo: 'blog/posts', pathMatch: 'full'},

  {path: 'auth', loadChildren: () => import ('./modules/auth/auth.module').then(module => module.AuthModule)},
  {path: 'blog',loadChildren: () => import ('./modules/blog/blog.module').then(module => module.BlogModule)},
  {path: 'admin', loadChildren: () => import ('./modules/admin/admin.module').then(module => module.AdminModule)},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
