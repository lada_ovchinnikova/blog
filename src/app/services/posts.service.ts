import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Post } from 'src/app/interfaces/post';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Postresponce } from '../interfaces/posts';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  // список всех постов
  find(): Observable<Post[]> {
    return this.httpClient.get<Postresponce>(`${environment.realtimeDBURL}/posts.json`).pipe(
    map((res: Postresponce) => Object
    .entries(res)
    .map(([id, post]: [string, Post]) => ({ ...post, id }))
    )
    )
    }
  // список всех постов
  delete(id: string) {
    return this.httpClient.delete<null>(`${environment.realtimeDBURL}/posts/${id}.json`)
    }

   // один пост по id
  findOne(id: string): Observable<Post>{
    return this.httpClient.get<Post>(`${environment.realtimeDBURL}/posts/${id}.json`)
    }
    
   // обновить по айди
  update (id: string, post: Post): Observable<Post>{
    return this.httpClient.put<Post>(`${environment.realtimeDBURL}/posts/${id}.json`, post)
    }
   // создать

  create(post: Post): Observable<Post>{
    return this.httpClient.post<{name: string}>(`${
      environment.realtimeDBURL}/posts.json`,
      post
    ).pipe(
      map((res: {name: string}) => ({ id: res.name, ...post }))
    );
  }
}
