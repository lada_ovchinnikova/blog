import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BlogModule } from './modules/blog/blog.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FbInterceptor } from './modules/auth/interceptors/fb.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    BlogModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: FbInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
